import { Injectable } from '@angular/core';
import { Http, RequestOptions ,Headers} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import { NavController } from 'ionic-angular';


@Injectable()
export class AuthServiceProvider {

  client_id:string = "2";
  client_secret:string = 'w2CRhJBYLAWXbzitqQ568YXCkwg8pMFIq9ya4U86';
  constructor (private _http:Http) {
  
  }
   public login(credentials) {
   // console.log("login ",credentials)
     if (credentials.email === null ||credentials.email === "" ||credentials.password === ""|| credentials.password === null) {
       return Observable.throw("Please insert credentials");
     } else {
         let headers = new Headers();
         headers.append("Accept", 'application/json');
         headers.append('Content-Type', 'application/json' );
         
         let options = new RequestOptions({ headers: headers } );
     
         let postParams = {
          'client_id' : this.client_id,
          'client_secret' : this.client_secret,
          'email': credentials.email,
          'password': credentials.password  
        } 
        let body = JSON.stringify(postParams);
        return this._http.post("https://webservice.musiqar.com/api/auth/login", body, options)
        .map(res => res.json())
        .do(data =>console.log(JSON.stringify(data)))
        .catch(this.handleError);
     }
   }
   public facebooklogin(access_token) {
    
      if (access_token === null ||access_token === "") {
        return Observable.throw("Please insert credentials");
      } else {
          let headers = new Headers();
          headers.append("Accept", 'application/json');
          headers.append('Content-Type', 'application/json' );
          
          let options = new RequestOptions({ headers: headers } );
      
          let postParams = {
           'client_id' : this.client_id,
           'client_secret' : this.client_secret,
           'access_token' : access_token
         } 
         let body = JSON.stringify(postParams);
         return this._http.post("https://webservice.musiqar.com/api/auth/fblogin", body, options)
         .map(res => res.json())
         .do(data =>console.log(JSON.stringify(data)))
         .catch(this.handleError);
      }
    }

handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}
  
   public register(credentials) {
   //  console.log(credentials)
     if (credentials.email === null || credentials.password === null) {
       return Observable.throw("Please insert credentials");
     } else {
      let headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      
      let options = new RequestOptions(
        { headers: headers }
       );
  
      let postParams = {
       'client_id' : this.client_id,
       'client_secret' : this.client_secret,
       'username': credentials.uname+"",
       'email': credentials.email+"",
       'password': credentials.password+""  ,
       
       'first_name': credentials.fname,
       'last_name': credentials.lname
     } 
     let body = JSON.stringify(postParams);
     return this._http.post("https://webservice.musiqar.com/api/auth/register", body, options)
     .map(res => res.json())
     .do(data =>console.log(JSON.stringify(data)))
     .catch(this.handleError);
     }
   }
   checkAuth ():number {
    var usrAuth = this.getUser();
    var yes = 1;
    if (!usrAuth.isLogged && !usrAuth.access_token) {
        this.destroyUserCredentials();
        //this.nav.setRoot(LoginPage);
        yes = 0;
    }
    return yes;
}

destroyUserCredentials() {  //for logout //remove from localstorage
    window.localStorage.removeItem('authorizationData');
}
  logout() {
    this.destroyUserCredentials();
    //this.nav.setRoot(LoginPage);
   }
  private getStorageVariable(name) {
    return JSON.parse(window.localStorage.getItem(name));
  }

  private setStorageVariable(name, data) {
    window.localStorage.setItem(name, JSON.stringify(data));
  }

  setUser(user_data) {
    window.localStorage.authorizationData = JSON.stringify(user_data);
};

getUser(){
    return JSON.parse(window.localStorage.authorizationData || '{}');
};

}


