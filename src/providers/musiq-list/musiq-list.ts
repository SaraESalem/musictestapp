import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

/*
  Generated class for the MusiqListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MusiqListProvider {
 private authData:any;
 private authToken:any;

  constructor(private _http: Http) {
    this.authData = JSON.parse(window.localStorage.authorizationData || '{}');
    console.log(this.authData);
   if (Object.keys(this.authData).length !== 0) { //logged in and has data
     this.authToken = this.authData.access_token
   }
       
  }
  public getMusiqList() {
    
    let headers = new Headers();   
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${this.authToken}`);
    let options = new RequestOptions({ headers: headers });
    
      return this._http.get('https://webservice.musiqar.com/api/tracks',options)
      .map(res => res.json())
      .do(data =>console.log(JSON.stringify(data)))
      .catch(this.handleError);
    
  }
  public getOneTrack(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${this.authToken}`);
    let options = new RequestOptions({ headers: headers });
    
       return this._http.get('https://webservice.musiqar.com/api/tracks/'+id,options)
       .map(res => res.json())
       .do(data =>console.log(JSON.stringify(data)))
       .catch(this.handleError);
     
   }
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}

}
