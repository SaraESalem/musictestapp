import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  createSuccess = false;
  registerCredentials = { 
    email: '', 
    password: '' ,
    fname:'',
    lname:'',
    uname:''
  };
 
  constructor(private nav: NavController, private auth: AuthServiceProvider, private alertCtrl: AlertController) { 
    console.log("register page")
  }
 
  public register() {
    this.auth.register(this.registerCredentials).subscribe(success => {
      console.log(success)
      if (success) {
        // "token_type": "Bearer",
        // "expires_in": 31536000,
        // "access_token": "Valid oAuth 2 Access Token"
        let user_data = {};
        user_data["isLogged"] = true;
        user_data["refresh_token"] = success.refresh_token;
        user_data["expires_in"] = success.expires_in;
        user_data["access_token"] = success.access_token;
        user_data["token_type"] = success.token_type;
        
       
        this.auth.setUser(user_data);
        
        this.createSuccess = true;
        this.nav.setRoot(HomePage);
        this.showPopup("Success", "Account created.");
        
      } else {
        this.showPopup("Error", "Problem creating account.");
      }
    },
      error => {
        this.showPopup("Error", error);
      });
  }
 
  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.nav.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }
}