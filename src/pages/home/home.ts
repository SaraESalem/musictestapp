import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController } from 'ionic-angular';
import { MusiqListProvider } from '../../providers/musiq-list/musiq-list';
import { AudioProvider } from 'ionic-audio';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  musicListData:any;
  loading: Loading;
  myTracks:any[] = [];
  allTracks: any[];

  constructor(public navCtrl: NavController,private musiclist:MusiqListProvider,private loadingCtrl: LoadingController,
     private alertCtrl: AlertController,private _audioProvider: AudioProvider,private auth: AuthServiceProvider,private nav: NavController) {
     let authparam = this.auth.checkAuth();
     if(authparam == 0 )this.nav.setRoot(LoginPage);
    this.loadData();
  }
  private loadData(){
   this.showLoading();
    this.musiclist.getMusiqList().subscribe(allowed => {
      if (allowed.success) {  
        for( let arr in allowed.list.data) {
           this.myTracks.push(allowed.list.data[arr]);
         
      }
      
       // this.musicListData = allowed.list.data;
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }
  // private playOneTrack(id){
  //   this.showLoading();
  //    this.musiclist.getOneTrack(id).subscribe(allowed => {
  //      console.log(allowed)
  //      if (allowed.success) {  
  //     //    for( let arr in allowed.list.data) {
  //     //       this.myTracks.push(allowed.list.data[arr]);
          
  //     //  }
       
  //     //   // this.musicListData = allowed.list.data;
  //     //  } else {
  //     //    this.showError("Access Denied");
  //     //  }
  //    },
  //      error => {
  //        this.showError(error);
  //      });
  //  }
  
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ngAfterContentInit() {     
    // get all tracks managed by AudioProvider so we can control playback via the API
    this.allTracks = this._audioProvider.tracks; 
  }
  
  // playSelectedTrack() {
  //   // use AudioProvider to control selected track 
  //   this._audioProvider.play(this.selectedTrack);
  // }
  
  // pauseSelectedTrack() {
  //    // use AudioProvider to control selected track 
  //    this._audioProvider.pause(this.selectedTrack);
  // }
         
  onTrackFinished(track: any) {
    console.log('Track finished', track)
  } 

}
