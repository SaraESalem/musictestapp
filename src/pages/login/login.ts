import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController, Platform } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';
import { Facebook } from '@ionic-native/facebook';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loading: Loading;
  registerCredentials = {
     email: '', 
     password: ''
     };
 //@Inject(AuthServiceProvider) private auth
  constructor(private nav: NavController, private alertCtrl: AlertController,private auth: AuthServiceProvider,
     private loadingCtrl: LoadingController,private facebook: Facebook,private platform: Platform) { 
       console.log("login")
     }
 
  public createAccount() {
    this.nav.push('RegisterPage');
  }
 
  public login() {
    this.showLoading()
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed) {   
        let user_data = {};
        
        user_data["isLogged"] = true;
        user_data["refresh_token"] = allowed.refresh_token;
        user_data["expires_in"] = allowed.expires_in;
        user_data["access_token"] = allowed.access_token;
        user_data["token_type"] = allowed.token_type;
       
        this.auth.setUser(user_data);  
        console.log(allowed)   
        this.nav.setRoot(HomePage);
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }
  fbLogin(){
    if (this.platform.is('cordova')) {
      this.facebook.login(['email']).then( (response) => {
        const facebookCredential = response.authResponse.accessToken;

        this.auth.facebooklogin(facebookCredential)
        .subscribe((success) => {
          let user_data = {};
          user_data["token"] = "";
          user_data["expires_in"] = "";
          user_data["access_token"] = "";
         
          this.auth.setUser(user_data);  
          console.log(success)   
          this.nav.setRoot(HomePage);
        },
        error => {
          this.showError(error);
        });

    }).catch((error) => { console.log(error) });
    }
   
}
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
